Source: ktikz
Section: graphics
Priority: optional
Maintainer: Debian KDE Extras Team <pkg-kde-extras@lists.alioth.debian.org>
Uploaders:
 Stuart Prescott <stuart@debian.org>,
 Florian Hackenberger <florian@hackenberger.at>
Build-Depends:
 cmake,
 debhelper (>= 11),
 extra-cmake-modules,
 libkf5doctools-dev,
 libkf5iconthemes-dev,
 libkf5parts-dev,
 libkf5texteditor-dev,
 libkf5xmlgui-dev,
 libpoppler-qt5-dev,
 libqt5sql5-sqlite,
 pkg-config,
 pkg-kde-tools,
 qtbase5-dev,
 qttools5-dev,
 qttools5-dev-tools,
 xsltproc
Homepage: https://github.com/fhackenberger/ktikz
Standards-Version: 4.2.1
Vcs-Git: https://salsa.debian.org/qt-kde-team/extras/ktikz.git
Vcs-Browser: https://salsa.debian.org/qt-kde-team/extras/ktikz

Package: ktikz
Architecture: any
Depends:
 preview-latex-style,
 texlive-pictures,
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 khelpcenter,
 poppler-utils
Description: editor for the TikZ drawing language - KDE version
 KtikZ is a small application to assist in the creation of diagrams and
 drawings using the TikZ macros from the LaTeX package "pgf". It consists of
 a text editor pane in which the TikZ code for the drawing is edited and a
 preview pane showing the drawing as rendered by LaTeX. The preview pane can be
 updated in real-time. Common drawing tools, options and styles are available
 from the menus to assist the coding process.
 .
 This package contains the KDE version of the program.
 .
 TikZ is a user-friendly syntax layer for the PGF (portable graphics format)
 TeX macro package. Pictures can be created within a LaTeX document and
 included in the output using the most important TeX backend drivers including
 pdftex and dvips.

Package: qtikz
Architecture: any
Depends:
 preview-latex-style,
 texlive-pictures,
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 poppler-utils,
 qttools5-dev-tools
Description: editor for the TikZ drawing language - Qt version
 QtikZ is a small application to assist in the creation of diagrams and
 drawings using the TikZ macros from the LaTeX package "pgf". It consists of
 a text editor pane in which the TikZ code for the drawing is edited and a
 preview pane showing the drawing as rendered by LaTeX. The preview pane can be
 updated in real-time. Common drawing tools, options and styles are available
 from the menus to assist the coding process.
 .
 This package contains the Qt version of the program.
 .
 TikZ is a user-friendly syntax layer for the PGF (portable graphics format)
 TeX macro package. Pictures can be created within a LaTeX document and
 included in the output using the most important TeX backend drivers including
 pdftex and dvips.
